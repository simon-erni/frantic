var storagePrefix = 'expansion-';

var playedCards = [];
var unplayedCards = [];

var currentCard = 0;

/**
 * Shuffles array in place.
 * @param {Array} a items An array containing the items.
 */
function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

function get(key) {
    return JSON.parse(localStorage.getItem(storagePrefix + key));
}

function put(key, value) {
    localStorage.setItem(storagePrefix + key, JSON.stringify(value));
}

function remove(key) {
    localStorage.removeItem(storagePrefix + key);
}

function reset() {

    remove("savedGame");
    location.reload();

}

function displayCard(card) {
    $("img.card").attr("src", "img/event"+card+".png");
    
    $(".events").children('section').each(function() {$(this).removeClass("block");});

    $("#event"+card).addClass("block");

}


function init() {
    if (get("savedGame") === true) {
        playedCards = get("playedCards");
        unplayedCards = get("unplayedCards");
        currentCard = get("currentCard");

        for (var i = 0; i < playedCards.length; i++) {
            addCardToPlayed(playedCards[i]);
        }

        displayCard(currentCard);


    } else {
        playedCards = [];
        unplayedCards = [];
    
        currentCard = 0;
    
        var n = $("section").length;
    
        for (var i = 1; i < n; i++) {
            unplayedCards.push(i);
        }
    
        shuffle(unplayedCards);
    }
}



function addCardToPlayed(card) {

    if (card === 0) {
        return;
    }

    if ($("div#alreadyplayed-wrap").hasClass("none")) {
        $("div#alreadyplayed-wrap").removeClass("none");
    }


    $("#alreadyplayed-cards").prepend("<div id='playedcard" + card + "' class='col-sm-3 col-xs-6 prepend'><img id='card"+card+"' class='played'></div>");
    $("img.played#card"+card).attr("src", "img/event"+card+".png");
}

function removeCardFromPlayed(card) {

    $("#playedcard" + card).remove();

    if ($("#alreadyplayed-cards").children().length === 0) {
        $("div#alreadyplayed-wrap").addClass("none");
    }

}

function saveState() {

    put("savedGame", true);
    put("playedCards", playedCards);
    put("unplayedCards", unplayedCards);
    put("currentCard", currentCard);
    
}

function gameOver() {
    $("#no-cards-alert").removeClass("none");

}

function playCard() {

    if (unplayedCards.length === 0) {
        
        gameOver();

        return;
    } 


    if (currentCard !== 0) {

        addCardToPlayed(currentCard);
        playedCards.push(currentCard);

    }

    currentCard = unplayedCards.pop();
    
    displayCard(currentCard);
    
    saveState();
};

function undo() {
    if (currentCard === 0) {
        return;
    }

    
    unplayedCards.push(currentCard);

    if (playedCards.length === 0) {
        currentCard = 0;
    } else {
        currentCard = playedCards.pop();
        removeCardFromPlayed(currentCard);
    }
    
    displayCard(currentCard);

    saveState();
}

init();