/**
 * Welcome to your Workbox-powered service worker!
 *
 * You'll need to register this file in your web app and you should
 * disable HTTP caching for this file too.
 * See https://goo.gl/nhQhGp
 *
 * The rest of the code is auto-generated. Please don't update this file
 * directly; instead, make changes to your Workbox build configuration
 * and re-run your build process.
 * See https://goo.gl/2aRDsh
 */

importScripts("https://storage.googleapis.com/workbox-cdn/releases/3.6.3/workbox-sw.js");

/**
 * The workboxSW.precacheAndRoute() method efficiently caches and responds to
 * requests for URLs in the manifest.
 * See https://goo.gl/S9QRab
 */
self.__precacheManifest = [
  {
    "url": "css/bootstrap.min.css",
    "revision": "2a2264a93775e8933f10a2f8ef00ce5e"
  },
  {
    "url": "css/demo.css",
    "revision": "69e9d6a95c03c500d8961e6a9ff9a22e"
  },
  {
    "url": "css/font-awesome.min.css",
    "revision": "861b93b125da96c853cb9680e0c844d2"
  },
  {
    "url": "css/icon.css",
    "revision": "99cba93c9195787796ab4162afe438a0"
  },
  {
    "url": "css/index.css",
    "revision": "0540ec23b6b606c67da8e1471c1feeea"
  },
  {
    "url": "css/material-kit.css",
    "revision": "89933dae6f4c97bd4cbf27f6c57f54f9"
  },
  {
    "url": "fav/android-icon-192x192.png",
    "revision": "987c63408473f900fafa80b14e769cbd"
  },
  {
    "url": "fav/apple-icon-114x114.png",
    "revision": "c2b34ad520e554b24ab6b40bf6de52bc"
  },
  {
    "url": "fav/apple-icon-120x120.png",
    "revision": "26ecaa2b2deda44bc1699ea16953d605"
  },
  {
    "url": "fav/apple-icon-144x144.png",
    "revision": "bb9747b1a10d02d23b4275ad03e3115a"
  },
  {
    "url": "fav/apple-icon-152x152.png",
    "revision": "e7ff05a08c81b65a73d82c86e5f6fa01"
  },
  {
    "url": "fav/apple-icon-180x180.png",
    "revision": "2c866412280b99e8e3aa74fae2a6162d"
  },
  {
    "url": "fav/apple-icon-57x57.png",
    "revision": "7647707f29c68bb72130c40efe5f4dd2"
  },
  {
    "url": "fav/apple-icon-60x60.png",
    "revision": "13d0a62ae96290ca4c2409af370c29dd"
  },
  {
    "url": "fav/apple-icon-72x72.png",
    "revision": "28e62ef03203f40436cbf02be0f8da5a"
  },
  {
    "url": "fav/apple-icon-76x76.png",
    "revision": "5d3accda2ec41f3ee0f8e53575988f33"
  },
  {
    "url": "fav/favicon-16x16.png",
    "revision": "7c41e92511bda7aa2374f2c069ebf383"
  },
  {
    "url": "fav/favicon-32x32.png",
    "revision": "e58bbe5f4796ff48bb4f9af785b588a9"
  },
  {
    "url": "fav/favicon-96x96.png",
    "revision": "0825a954cd636abd5e8f8185a1269d10"
  },
  {
    "url": "fav/manifest.json",
    "revision": "ea09f71c33c90350f612b2e73cdb9ca8"
  },
  {
    "url": "fonts/flUhRq6tzZclQEJ-Vdg-IuiaDsNcIhQ8tQ.woff2",
    "revision": "8a9a261c8b8dfe90db11f1817a9d22e1"
  },
  {
    "url": "fonts/fontawesome-webfont.eot",
    "revision": "674f50d287a8c48dc19ba404d20fe713"
  },
  {
    "url": "fonts/fontawesome-webfont.svg",
    "revision": "912ec66d7572ff821749319396470bde"
  },
  {
    "url": "fonts/fontawesome-webfont.ttf",
    "revision": "b06871f281fee6b241d60582ae9369b9"
  },
  {
    "url": "fonts/fontawesome-webfont.woff",
    "revision": "fee66e712a8a08eef5805a46892932ad"
  },
  {
    "url": "fonts/fontawesome-webfont.woff2",
    "revision": "af7ae505a9eed503f8b8e6982036873e"
  },
  {
    "url": "fonts/FontAwesome.otf",
    "revision": "0d2717cd5d853e5c765ca032dfd41a4d"
  },
  {
    "url": "fonts/roboto-v18-latin-300.eot",
    "revision": "548ebe05978f34db74a97d9e9c0bbf3a"
  },
  {
    "url": "fonts/roboto-v18-latin-300.svg",
    "revision": "dd0bea1f9a808d633492fa573039ca1d"
  },
  {
    "url": "fonts/roboto-v18-latin-300.ttf",
    "revision": "ab2789c48bf32d301cc7bb16677fb703"
  },
  {
    "url": "fonts/roboto-v18-latin-300.woff",
    "revision": "a1471d1d6431c893582a5f6a250db3f9"
  },
  {
    "url": "fonts/roboto-v18-latin-300.woff2",
    "revision": "55536c8e9e9a532651e3cf374f290ea3"
  },
  {
    "url": "fonts/roboto-v18-latin-500.eot",
    "revision": "03bb29d6722bf52f7fe88a6ed47d9e6e"
  },
  {
    "url": "fonts/roboto-v18-latin-500.svg",
    "revision": "95204ac95130828753c0ee0ada537c33"
  },
  {
    "url": "fonts/roboto-v18-latin-500.ttf",
    "revision": "4d88404f733741eaacfda2e318840a98"
  },
  {
    "url": "fonts/roboto-v18-latin-500.woff",
    "revision": "de8b7431b74642e830af4d4f4b513ec9"
  },
  {
    "url": "fonts/roboto-v18-latin-500.woff2",
    "revision": "285467176f7fe6bb6a9c6873b3dad2cc"
  },
  {
    "url": "fonts/roboto-v18-latin-700.eot",
    "revision": "376e0950b361fbd3b09508031f498de5"
  },
  {
    "url": "fonts/roboto-v18-latin-700.svg",
    "revision": "57888be7f3e68a7050452ea3157cf4de"
  },
  {
    "url": "fonts/roboto-v18-latin-700.ttf",
    "revision": "cae5027f600d2a0d88ac309655618e31"
  },
  {
    "url": "fonts/roboto-v18-latin-700.woff",
    "revision": "cf6613d1adf490972c557a8e318e0868"
  },
  {
    "url": "fonts/roboto-v18-latin-700.woff2",
    "revision": "037d830416495def72b7881024c14b7b"
  },
  {
    "url": "fonts/roboto-v18-latin-regular.eot",
    "revision": "68889c246da2739681c1065d15a1ab0b"
  },
  {
    "url": "fonts/roboto-v18-latin-regular.svg",
    "revision": "8681f434273fd6a267b1a16a035c5f79"
  },
  {
    "url": "fonts/roboto-v18-latin-regular.ttf",
    "revision": "372d0cc3288fe8e97df49742baefce90"
  },
  {
    "url": "fonts/roboto-v18-latin-regular.woff",
    "revision": "bafb105baeb22d965c70fe52ba6b49d9"
  },
  {
    "url": "fonts/roboto-v18-latin-regular.woff2",
    "revision": "5d4aeb4e5f5ef754e307d7ffaef688bd"
  },
  {
    "url": "img/event0.png",
    "revision": "8649149d690842ace8287bdc136ceea6"
  },
  {
    "url": "img/event1.png",
    "revision": "d2b30ebfe678f02452e977fa0ab726e5"
  },
  {
    "url": "img/event10.png",
    "revision": "ad93854e59d49c657b855c18e566e5d1"
  },
  {
    "url": "img/event11.png",
    "revision": "2cd721b44d4f5d501da8a0784c8aef13"
  },
  {
    "url": "img/event12.png",
    "revision": "7e7744f8c8d28c42c8a4cc859760574a"
  },
  {
    "url": "img/event13.png",
    "revision": "bce553ae7c1267bdb11c4b2efb6a31ca"
  },
  {
    "url": "img/event14.png",
    "revision": "c547b76b77936e1d3f52878f6a9672e5"
  },
  {
    "url": "img/event15.png",
    "revision": "a9ec0dd1a27eeb1ea51c265e4898bdcd"
  },
  {
    "url": "img/event16.png",
    "revision": "1ba5b9739b6b273547ce67172b49693a"
  },
  {
    "url": "img/event17.png",
    "revision": "ab565149de57018912d5906d8ebfe60c"
  },
  {
    "url": "img/event18.png",
    "revision": "845126b85ec6d83d114cf2c5abcef4de"
  },
  {
    "url": "img/event19.png",
    "revision": "da7d5809bfa234b136c8787056d31844"
  },
  {
    "url": "img/event2.png",
    "revision": "4b06671946d53310ad7c74d319422717"
  },
  {
    "url": "img/event20.png",
    "revision": "fc600776ac1daceafe38d5e676e2d577"
  },
  {
    "url": "img/event3.png",
    "revision": "2b6f01fd80ddb9c29768e25dfde3b0af"
  },
  {
    "url": "img/event4.png",
    "revision": "e8fb1bd0ca927f3c34d36a757a147333"
  },
  {
    "url": "img/event5.png",
    "revision": "5dc7d43397a4d69ebbaae7200c9ef9df"
  },
  {
    "url": "img/event6.png",
    "revision": "5a832df0036911ea172696ebb3338c58"
  },
  {
    "url": "img/event7.png",
    "revision": "62b35c5c00da892985a65534ea3e874e"
  },
  {
    "url": "img/event8.png",
    "revision": "ce1278b2b22f432edcddaab355164fc9"
  },
  {
    "url": "img/event9.png",
    "revision": "78400778b6883d013af64462bced1970"
  },
  {
    "url": "img/Logo_Rulefactory_white_200px.png",
    "revision": "aa6aadf6a0f9ba335dc58565a7e54bbf"
  },
  {
    "url": "index.html",
    "revision": "d206894a1534f60b3b925a19e85f81b4"
  },
  {
    "url": "js/bootstrap-datepicker.js",
    "revision": "b111629a5d50b6135bd3a2f04eb81103"
  },
  {
    "url": "js/bootstrap.min.js",
    "revision": "4becdc9104623e891fbb9d38bba01be4"
  },
  {
    "url": "js/index.js",
    "revision": "63507d32a62f6add4c7de383b1b196bf"
  },
  {
    "url": "js/jquery.min.js",
    "revision": "e071abda8fe61194711cfc2ab99fe104"
  },
  {
    "url": "js/material-kit.js",
    "revision": "f6d04f59ae7bac715d55dfe618f223b7"
  },
  {
    "url": "js/material.min.js",
    "revision": "bd26a69c4211d42518ba6dbc8ea67da7"
  },
  {
    "url": "js/nouislider.min.js",
    "revision": "68309968fd36260a4a2c2171987e5766"
  }
].concat(self.__precacheManifest || []);
workbox.precaching.suppressWarnings();
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});
