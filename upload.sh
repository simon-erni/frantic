#!/bin/bash

workbox generateSW workbox-config.normal.js
workbox generateSW workbox-config.expansion.js

rsync --progress --exclude=.DS_Store --archive --verbose --checksum --compress --delete -e ssh expansion/ "ernisi3"@"s63.server.hostpoint.ch":"/home/ernisi3/www/erni.io/frantic/"
rsync --progress --exclude=.DS_Store --archive --verbose --checksum --compress --delete -e ssh normal/ "ernisi3"@"s63.server.hostpoint.ch":"/home/ernisi3/www/erni.io/frantic2/"