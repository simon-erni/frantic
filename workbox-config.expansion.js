module.exports = {
  "globDirectory": "expansion",
  "globPatterns": [
    "**/*.{css,png,json,woff2,eot,svg,ttf,woff,otf,html,js}"
  ],
  "swDest": "expansion/sw.js"
};