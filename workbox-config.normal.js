module.exports = {
  "globDirectory": "normal",
  "globPatterns": [
    "**/*.{css,png,json,woff2,eot,svg,ttf,woff,otf,html,js}"
  ],
  "swDest": "normal/sw.js"
};